# How to install the application

1. Clone the repository `git clone https://qonand@bitbucket.org/qonand/homework3.git`
2. Run `docker-compose up -d workspace` in `laradock` folder
3. Run `docker-compose exec workspace bash` in `laradock` folder and in opened bash run the following commands:

 - `composer install`
 - `php artisan key:generate`

4. Set value of `GA_TRACKING_ID` to .env file in `app` folder

# How to run the application
1. Run `docker-compose exec workspace bash` in `laradock` folder 
2. In opened bash run the following command `php artisan currency-rate:update`

# Result
![Scheme](example.png)