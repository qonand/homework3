<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Irazasyed\LaravelGAMP\Facades\GAMP;

class UpdateCurrencyRate extends Command
{
    /**
     * @var string
     */
    private const RATE_SOURCE = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json';

    /**
     * @var int
     */
    private const INTERVAL_BETWEEN_REQUESTS = 5;

    /**
     * {@inheritdoc}
     */
    protected $signature = 'currency-rate:update';

    /**
     * {@inheritdoc}
     */
    protected $description = 'Update currency rate and push it to GA';

    /**
     * {@inheritdoc}
     */
    public function handle(): int
    {
        $gamp = GAMP::setClientId('123456');
        while (true) {
            $response = Http::get(static::RATE_SOURCE);

            if (!$response->ok()) {
                $this->error('Server is unavailable. Try later');
                continue;
            }

            $rateInfo = $response->collect()->firstWhere('cc', 'USD');
            if (!array_key_exists('rate', $rateInfo)) {
                $this->error('Rate is not correct. Try later');
                continue;
            }

            $rate = $rateInfo['rate'];
            $this->info("Current rate uah\usd $rate");

            $gamp->setEventCategory('Currency rate')
                ->setEventAction('Create')
                ->setEventValue((string) $rate)
                ->setEventLabel('Current currency rate')
                ->sendEvent();

            sleep(static::INTERVAL_BETWEEN_REQUESTS);
        }
    }
}
